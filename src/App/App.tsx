import React, { Component } from 'react';
import { Recipe } from '../Models/Recipe';
import { RecipeService } from '../Services/RecipeService';
import { AppProps, AppState } from './App.types';
import { Layout } from './Layouts/Layout';
import { RecipeView } from './Views/Recipe/Recipe';
import { RecipesView } from './Views/Recipes/Recipes';

class App extends Component<AppProps, AppState> {
  recipeService: RecipeService;

  set Recipes(recipes: Array<Recipe>) { this.setState({ recipes }); }
  get Recipes(): Array<Recipe> { return this.state.recipes; }

  get Recipe(): Recipe { return this.state.recipe; }
  set Recipe(recipe: Recipe) { this.setState({ recipe }); }

  get Errors(): Array<Error> { return this.state.errors; }

  static defaultProps = {
    loadRecipes: () => []
  }

  constructor(props) {
    super(props);
    this.state = { recipe: null, recipes: [], errors: [] };
    this.loadRecipes();
  }

  componentDidUpdate() {
    try {
      this.props.loadRecipes();
    }
    catch(error) {
      this.addError(error);
    }
  }

  onRecipeSelect = (recipe: Recipe) => {
    try {
      this.Recipe = recipe;
    }
    catch(error) {
      this.addError(error);
    }
  }

  onRecipeReturnClick = () => {
    try {
      this.Recipe = null;
      this.loadRecipes();
    }
    catch(error) {
      this.addError(error);
    }
  }

  render() {
    return (
      <Layout>
        {this.renderRecipes()}
        {this.renderErrors()}
      </Layout>
    )
  }

  private renderRecipes() {
    if(this.Recipe) {
      return (<RecipeView recipe={this.Recipe} onReturnClick={this.onRecipeReturnClick}/>);
    }

    return (
      <RecipesView recipes={this.Recipes} onSelect={this.onRecipeSelect} />
    );
  }

  private renderErrors() {
    if(!this.Errors.length) {
      return null;
    }

    return this.Errors.map((error, index) =>
      <div key={index} className="error">{error}</div>
    );
  }

  private addError(error: Error) {
    const updatedErrors = [...this.Errors, error];

    this.setState({ errors: updatedErrors });
  }

  private async loadRecipes(): Promise<void> {
    this.Recipes = await this.props.loadRecipes();
  }
}

export default App;
