import { createMuiTheme, Grid, makeStyles, MuiThemeProvider, Paper, responsiveFontSizes } from '@material-ui/core';
import DescriptionIcon from '@material-ui/icons/Description';
import GitHubIcon from '@material-ui/icons/GitHub';
import React from 'react';
import logo from '../../Assets/logo.svg';
import { styles } from './Layout.styles';

const useStyles = makeStyles(styles);
const theme = responsiveFontSizes(createMuiTheme(), { factor: 0.1 });

export const Layout = (props) => {

  const classes = useStyles();
  return (
    <MuiThemeProvider theme={theme}>
      <Grid>
        <Grid container direction="row" alignItems="center" component={Paper} justify="space-between" className={classes.Header}>
          <Grid item className={classes.Logo} xs={6}>
            <img alt={logo} src={logo} /><span>Recipes</span>
          </Grid>
          <Grid container item direction="row" justify="space-around" className={classes.Icons} xs={1}>
            <a href="https://github.com/imarcelolz/marleyspoon-recipes"><GitHubIcon /></a>
            <a href="https://imarcelolz.github.io"><DescriptionIcon /></a>
          </Grid>
        </Grid>
        <Grid container item md={12} className={classes.Content}>
          {props.children}
        </Grid>
      </Grid>
    </MuiThemeProvider>
  );
}
