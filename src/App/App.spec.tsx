import { mount, shallow } from 'enzyme';
import React from 'react';
import { recipesMock } from '../Mocks/Recipes';
import App from './App';
import { RecipeView } from './Views/Recipe/Recipe';
import { RecipesView } from './Views/Recipes/Recipes';

let wrapper;
const recipes = recipesMock;
const loadRecipes = () => Promise.resolve(recipes);

describe('Context: Rendering', () => {
  it('render without crash with default props', () => {
    wrapper = shallow(<App />);

    expect(wrapper).toMatchSnapshot();
  });

  it('render with recipes', () => {
    wrapper = shallow(<App loadRecipes={loadRecipes} />);

    expect(wrapper).toMatchSnapshot();
  });
});


describe('Context: Functional', () => {
  let recipesView, recipeView, recipe;

  beforeEach(() => {
    recipe = recipes[0];

    wrapper = mount((<App loadRecipes={loadRecipes} />));
    recipesView = wrapper.find(RecipesView);
    recipeView = wrapper.find(RecipeView);
  });

  it('show the recipe list', () => {
    expect(wrapper.exists(RecipesView)).toBeTruthy();
    expect(wrapper.exists(RecipeView)).toBeFalsy();
  });

  it('show the recipe', () => {
    wrapper.setState({ recipe: recipe });

    expect(wrapper.exists(RecipesView)).toBeFalsy();
    expect(wrapper.exists(RecipeView)).toBeTruthy();
  });

  it('show the selected recipe when it is selected', () => {
    recipesView.invoke('onSelect')(recipe);

    recipeView = wrapper.find(RecipeView);
    expect(wrapper.state('recipe')).toEqual(recipe);
    expect(recipeView.prop('recipe')).toEqual(recipe);
  });

  it('back to list when recipe view exit', () => {
    wrapper.setState({ recipe: recipes });

    recipeView = wrapper.find(RecipeView)
    recipeView.invoke('onReturnClick')({});

    expect(wrapper.exists(RecipesView)).toBeTruthy();
    expect(wrapper.exists(RecipeView)).toBeFalsy();
  });

  xit('display errors', () => {
    const loadRecipes = () => Promise.resolve('nice error message');
    wrapper.setProps({ loadRecipes });
    wrapper.update();

    expect(wrapper.exists('.error')).toBeTruthy();
  });
});
