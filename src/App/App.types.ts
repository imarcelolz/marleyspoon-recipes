import { Recipe } from '../Models/Recipe';

export interface AppProps {
  loadRecipes: () => Promise<Recipe[]>;
}

export interface AppState {
  recipes: Array<Recipe>;
  recipe?: Recipe;
  errors: Array<Error>;
}
