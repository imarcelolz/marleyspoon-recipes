import { Recipe } from '../../../Models/Recipe';

export interface RecipeViewProps {
  recipe: Recipe,
  onReturnClick: () => void,
}
