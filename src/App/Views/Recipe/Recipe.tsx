import { Button, Chip, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import React from 'react';
import ReactMarkdown from 'react-markdown';
import { styles } from './Recipe.styles';
import { RecipeViewProps } from './Recipe.types';

const useStyles = makeStyles(styles);
export function RecipeView(props: RecipeViewProps) {
  const { recipe, onReturnClick } = props;
  const classes = useStyles();

  if(!recipe) { return null; }

  const tags = recipe.tags?.map((tag: string) =>
    <Chip label={tag} variant="outlined" className={classes.Tag}/>
  );

  return (
    <Grid container justify="center">
      <Grid container item xs={8} spacing={2} component={Paper}>
        <Grid item>
          <img src={recipe.image} className={classes.Image} alt={recipe.title}/>
        </Grid>
        <Grid item>
          <Typography variant="h4">{recipe.title}</Typography>
        </Grid>
        <Grid item>
          <ReactMarkdown source={recipe.description} />
        </Grid>
        <Grid item>{tags}</Grid>
        <Grid container item justify="space-between">
          <Typography variant="h4">{recipe.chef || 'Family Recipe'}</Typography>
          <Button onClick={onReturnClick}>Back</Button>
        </Grid>
      </Grid>
    </Grid>
  );
}

RecipeView.defaultProps = {
  recipe: null,
  onReturnClick: () => {},
}
