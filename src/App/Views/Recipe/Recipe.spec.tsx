import { Button } from '@material-ui/core';
import { mount, shallow } from 'enzyme';
import React from 'react';
import { recipesMock } from '../../../Mocks/Recipes';
import { RecipeView } from './Recipe';

let wrapper;
const recipe = recipesMock[0];
describe('Context: Rendering', () => {
  it('render without crash with default props', () => {
    wrapper = shallow(<RecipeView />);

    expect(wrapper).toMatchSnapshot();
  });

  it('render with one recipe', () => {
    wrapper = shallow(<RecipeView recipe={recipe}/>);

    expect(wrapper).toMatchSnapshot();
  });
});


describe('Context: Functional', () => {
  it('trigger onReturnClick when button is clicked', () => {
    const mock = jest.fn();

    wrapper = mount(<RecipeView recipe={recipe} onReturnClick={mock}/>)
    wrapper.find(Button).first().invoke('onClick')({});

    expect(mock).toHaveBeenCalledTimes(1);
  });
});
