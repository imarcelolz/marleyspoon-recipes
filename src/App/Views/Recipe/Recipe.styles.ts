export const styles = {
  Image: {
    width: '100%'
  },
  Tag: {
    'text-transform': 'capitalize',
    marginRight: '5px'
  }
};
