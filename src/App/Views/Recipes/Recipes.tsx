import { Button, Card, CardActions, CardContent, CardMedia, Grid, makeStyles, Typography } from '@material-ui/core';
import React, { Fragment } from 'react';
import { styles } from './Recipes.styles';
import { RecipesViewProps } from './Recipes.types';

const useStyles = makeStyles(styles);
export function RecipesView(props: RecipesViewProps) {
  const classes = useStyles();
  const items = props.recipes.map((recipe, index) =>
    <Grid key={index} item md={4}>
      <RecipeCard recipe={recipe} onClick={() => props.onSelect(recipe) } />
    </Grid>
  );

  return (
    <Fragment>
      <Typography variant="h2" className={classes.Header}>Our weekly menu</Typography>
      <Grid container spacing={3}>
        {items}
      </Grid>
    </Fragment>
  )
}

RecipesView.defaultProps = {
  recipes: [],
  onSelect: () => {}
}


const RecipeCard = ({ recipe, onClick }) => {
  const classes = useStyles();

  return (
    <Card className={'as'}>
      <CardMedia className={classes.RecipeMedia} image={recipe.image} /><CardContent>
        <Typography variant="body1" color="textSecondary" component="p" className={classes.RecipeTitle}>
          {recipe.title}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <Button onClick={onClick}>
          See More
        </Button>
      </CardActions>
    </Card>
  );
}
