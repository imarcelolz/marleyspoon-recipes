import { Recipe } from '../../../Models/Recipe';

export interface RecipesViewProps {
  recipes: Array<Recipe>;
  onSelect: (recipe: Recipe) => void;
}
