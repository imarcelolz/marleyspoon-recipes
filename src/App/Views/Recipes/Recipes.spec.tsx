import { Button } from '@material-ui/core';
import { mount, shallow } from 'enzyme';
import React from 'react';
import { recipesMock } from '../../../Mocks/Recipes';
import { RecipesView } from './Recipes';

let wrapper;
const recipes = recipesMock;
describe('Context: Rendering', () => {
  it('render without crash with default props', () => {
    wrapper = shallow(<RecipesView />);

    expect(wrapper).toMatchSnapshot();
  });

  it('render with recipes', () => {
    wrapper = shallow(<RecipesView recipes={recipes}/>);

    expect(wrapper).toMatchSnapshot();
  });
});


describe('Context: Functional', () => {
  it('trigger onSelect one recipe is selected', () => {
    const mock = jest.fn();

    wrapper = mount(<RecipesView recipes={recipes} onSelect={mock} />)
    wrapper.find(Button).first().invoke('onClick')({});

    expect(mock).toHaveBeenCalledTimes(1);
  });
});
