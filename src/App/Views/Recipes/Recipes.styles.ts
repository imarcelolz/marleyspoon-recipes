export const styles = {
  Header: {
    marginBottom: '15px'
  },
  RecipeMedia: {
    paddingTop: '56.25%',
  },
  RecipeTitle: {
    'white-space': 'nowrap',
    overflow: 'hidden',
    'text-overflow': 'ellipsis'
  }
};
