export interface Recipe {
  id: string;
  title: string;
  image: string;
  tags?: Array<string>;
  description: string;
  chef?: string;
  calories: number;
  createdAt: string;
  updatedAt: string;
}
