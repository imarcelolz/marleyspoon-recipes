import { ContentfulClientApi } from 'contentful';

export const contentfulClientApiFactory = (): ContentfulClientApi => {
  const getEntries = jest.fn();

  getEntries.mockResolvedValueOnce(getEntriesResponse);

  return { getEntries } as unknown as ContentfulClientApi;
};

export const chefMock = { sys: { id: '1Z8SwWMmS8E84Iogk4E6ik' }, fields: { name: 'Mark Zucchiniberg ' } };
export const tagMock =  { sys: { id: '61Lgvo6rzUIgIGgcOAMgQ8', }, fields: { name: 'gluten free' } };
export const assetMock = { sys: { id: '61XHcqOBFYAYCGsKugoMYK' }, fields: { file: { url: '//images.ctfassets.net/kk2bw5ojx476/61XHcqOBFYAYCGsKugoMYK/0009ec560684b37f7f7abadd66680179/SKU1240_hero-374f8cece3c71f5fcdc939039e00fb96.jpg' }}};

export const recipeResponseMock = {
  sys: { id: '4dT8tcb6ukGSIg2YyuGEOm', createdAt: '2018-05-07T13:38:22.351Z', updatedAt: '2018-05-07T13:41:01.986Z' },
  fields: {
    title: 'White Cheddar Grilled Cheese with Cherry Preserves & Basil',
    photo: { sys: { id: assetMock.sys.id } },
    calories: 788,
    description: '*Grilled Cheese 101*: Use delicious cheese and good quality bread; make crunchy on the outside and ooey gooey on the inside; add one or two ingredients for a flavor punch! In this case, cherry preserves serve as a sweet contrast to cheddar cheese, and basil adds a light, refreshing note. Use __mayonnaise__ on the outside of the bread to achieve the ultimate, crispy, golden-brown __grilled cheese__. Cook, relax, and enjoy!',
    tags: [ { sys: { id: tagMock.sys.id } } ],
    chef: { sys: { id: chefMock.sys.id } }
  }
};

export const partialRecipeResponseMock = {
  sys: { id: '4dT8tcb6ukGSIg2YyuGEOm', createdAt: '2018-05-07T13:38:22.351Z', updatedAt: '2018-05-07T13:41:01.986Z' },
  fields: {
    title: 'White Cheddar Grilled Cheese with Cherry Preserves & Basil',
    photo: null,
    calories: 788,
    description: '*Grilled Cheese 101*: Use delicious cheese and good quality bread; make crunchy on the outside and ooey gooey on the inside; add one or two ingredients for a flavor punch! In this case, cherry preserves serve as a sweet contrast to cheddar cheese, and basil adds a light, refreshing note. Use __mayonnaise__ on the outside of the bread to achieve the ultimate, crispy, golden-brown __grilled cheese__. Cook, relax, and enjoy!',
    tags: null,
    chef: null
  }
};

export const getEntriesResponse = {
  sys: { type: 'Array' },
  total: 1,
  skip: 0,
  limit: 100,
  items: [recipeResponseMock],
  includes: {
    Entry: [ chefMock, tagMock ],
    Asset: [assetMock]
  }
}

export const getEntriesPartialResponse = {
  sys: { type: 'Array' },
  total: 1,
  skip: 0,
  limit: 100,
  items: [partialRecipeResponseMock],
  includes: {}
}

export const getEntriesError = {
  sys: { type: 'Error', id: 'AccessTokenInvalid' },
  message: 'Nice erorr message.',
  requestId: '68a2da44-ca1d-4864-bf13-3e699b7d6ec0'
}
