import { mocked } from 'ts-jest/utils';
import { contentfulClientApiFactory, getEntriesError, getEntriesPartialResponse } from '../Mocks/ContentfulApiMocks';
import { RecipeService } from './RecipeService';

let target: RecipeService = null;
let apiClient;
let apiClientMock;

beforeEach(() => {
  apiClient = contentfulClientApiFactory();

  apiClientMock = mocked(apiClient);
  target = new RecipeService(apiClient);
});

describe('#index', () => {
  it('calls the api with correct params', async () => {
    await target.recipes();

    expect(apiClient.getEntries).toHaveBeenCalledWith({ 'content_type': 'recipe' });
  });

  it('loads all recipes', async () => {
    const expected = ['White Cheddar Grilled Cheese with Cherry Preserves & Basil'];
    const recipes = await target.recipes();
    const recipesTitles = recipes.map((recipe) => recipe.title);

    expect(recipesTitles).toEqual(expected)
  });

  it('parse recipes with missing data', async () => {
    apiClientMock.getEntries.mockResolvedValueOnce(getEntriesPartialResponse);

    const expected = ['White Cheddar Grilled Cheese with Cherry Preserves & Basil'];
    const recipes = await target.recipes();
    const recipesTitles = recipes.map((recipe) => recipe.title);

    expect(recipesTitles).toEqual(expected)
  });

  it('threat errors and send it to app gracefully', () => {
    apiClientMock.getEntries.mockRejectedValueOnce(getEntriesError);

    return target.recipes().catch((error) => {
      expect(error).toEqual('');
    });
  });
});
