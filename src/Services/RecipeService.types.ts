export interface ApiSys {
  type: string;
  linkType: string;
  id: string;
}

export interface ApiLink {
  sys: ApiSys
}

export interface ApiRecipe {
  title: string;
  photo: ApiLink;
  calories: number;
  description: string;
  chef: ApiLink;
  tags: ApiLink[];
}
