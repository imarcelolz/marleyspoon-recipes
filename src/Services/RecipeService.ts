import { ContentfulClientApi, Entry } from 'contentful';
import { Recipe } from '../Models/Recipe';
import { ApiLink, ApiRecipe } from './RecipeService.types';

export class RecipeService {
  constructor(private apiClient: ContentfulClientApi) {}

  recipes = async (): Promise<Recipe[]> => {
    try {
      const response = await this.apiClient.getEntries<ApiRecipe>({ 'content_type': 'recipe' });

      return response.items.map((item) => RecipeService.entryToRecipe(item, response.includes));

     } catch(error) {
        throw error.message || error;
    }
  }

  private static entryToRecipe(entry: Entry<ApiRecipe>, includes: any): Recipe {
    const entries = [ ...includes['Entry'], ...includes['Asset']];

    const resolveInclude = (link: ApiLink) => {
      if(!link) { return null; }

      const entry = entries.find(item => item.sys.id === link.sys.id);

      return entry?.fields;
    };

    const chef = resolveInclude(entry.fields.chef);
    const tags = entry.fields.tags?.map((tag) => resolveInclude(tag));
    const photo = resolveInclude(entry.fields.photo)

    return{
      id: entry.sys.id,
      title: entry.fields.title,
      calories: entry.fields.calories,
      description: entry.fields.description,
      createdAt: entry.sys.createdAt,
      updatedAt: entry.sys.updatedAt,
      chef: chef?.name,
      tags: tags?.map((tag) => tag.name),
      image: photo?.file?.url
    };
  }
}
