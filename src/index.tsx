import { createClient } from 'contentful';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App/App';
import './index.css';
import { RecipeService } from './Services/RecipeService';
import * as serviceWorker from './serviceWorker';
const api = createClient({ space: 'kk2bw5ojx476', accessToken: '7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c' });
const recipeService = new RecipeService(api);

ReactDOM.render(
  <React.StrictMode>
    <App loadRecipes={recipeService.recipes} />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
